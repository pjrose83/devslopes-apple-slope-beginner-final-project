//
//  DetailVC.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    @IBOutlet weak var detailBgImg: UIImageView!
    @IBOutlet weak var detailMainImg: UIImageView!
    @IBOutlet weak var detailTitleLbl: UILabel!
    @IBOutlet weak var detailDescriptionLbl: UILabel!
    
    private var img = ""
    private var detTitle = ""
    private var desc = ""
    
    func initDetail(specificCat: SpecificCat) {
        detTitle = specificCat.title
        img = specificCat.imageName
        desc = specificCat.description
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailBgImg.image = UIImage(named: imageFileName)
        detailMainImg.image = UIImage(named: img)
        detailTitleLbl.text = detTitle
        detailDescriptionLbl.text = desc
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
