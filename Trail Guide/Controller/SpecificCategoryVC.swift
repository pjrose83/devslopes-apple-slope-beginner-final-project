//
//  SpecificCategoryVC.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import UIKit

class SpecificCategoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var specificCatCollection: UICollectionView!
    @IBOutlet weak var specificCatBG: UIImageView!
    @IBOutlet weak var specificCatLbl: UILabel!
    
    private(set) public var specificCats = [SpecificCat]()
    
    func initSpecificCats(category: Category){
        specificCats = DataService.instance.getSpecificCat(forCategoryTitle: category.title)
        labelText = category.title
        imageFileName = category.imageName
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        specificCatCollection.delegate = self
        specificCatCollection.dataSource = self
        
        specificCatBG.image = UIImage(named: imageFileName)
        specificCatLbl.text = "\(labelText)\nGEAR"
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return specificCats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecificCatCell", for: indexPath) as? SpecificCatCell {
                let specificCat = specificCats[indexPath.row]
            cell.updateViews(specificCat: specificCat)
            return cell
        }
        return SpecificCatCell()
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = specificCats[indexPath.row]
        performSegue(withIdentifier: "toDetailVC", sender: detail)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailVC {
            detailVC.initDetail(specificCat: sender as! SpecificCat)
        }
    }

    @IBAction func unwindToSpecificCategory(sender: UIStoryboardSegue){}

}
