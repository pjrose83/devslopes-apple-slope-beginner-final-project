//
//  Category.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import Foundation

struct Category {
    private(set) var title: String
    private(set) var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
