//
//  DataService.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(title: "HIKING", imageName: "hikingBG.png"),
        Category(title: "CAMPING", imageName: "campingBG.png"),
        Category(title: "DIVING", imageName: "divingBG.png"),
        Category(title: "RV LIFE", imageName: "rvBG.png"),
        Category(title: "FISHING", imageName: "fishingBG.png"),
        Category(title: "BACKPACKS", imageName: "backpackingBG.png")
    ]
    
    func getCategories() -> [Category] {
        return categories
    }
    
    private let campingGear = [
        SpecificCat(title: "SLEEPING BAG", imageName: "sleepingBag.png", description: "A sleeping bag is a camping essential. It provides a warm and semi-comfortable place to sleep when camping. Some sleeping bags are rated for temperatures below 0\u{00B0}F!\n\nWe reccomend the SnugSleeper SubZero bag for camping anywhere you think you might be cold."),
        SpecificCat(title: "PUP TENT", imageName: "pupTent.png", description: "A quick and easy-to-assemble shelter."),
        SpecificCat(title: "FOOD COOLER", imageName: "foodCooler.png", description: "Keeps your beer nice and chilly!"),
        SpecificCat(title: "CAMPING STOVE", imageName: "campingStove.png", description: "For all your cooking needs."),
        SpecificCat(title: "TACTICAL KNIFE", imageName: "tacticalKnife.png", description: "You call that a knife?..."),
        SpecificCat(title: "PARACORD BRACELET", imageName: "paracordBracelet.png", description: "???")
    ]
    
    private let hikingGear = [SpecificCat]()
    
    private let divingGear = [SpecificCat]()
    
    private let rvLifeGear = [SpecificCat]()
    
    private let fishingGear = [SpecificCat]()
    
    private let backpackingGear = [SpecificCat]()
    
    func getCampingGear() -> [SpecificCat] {
        return campingGear
    }
    
    func getHikingGear() -> [SpecificCat] {
        return hikingGear
    }
    
    func getDivingGear() -> [SpecificCat] {
        return divingGear
    }
    
    func getRvLifeGear() -> [SpecificCat] {
        return rvLifeGear
    }
    
    func getFishingGear() -> [SpecificCat] {
        return fishingGear
    }
    
    func getBackpackingGear() -> [SpecificCat] {
        return backpackingGear
    }
    
    func getSpecificCat(forCategoryTitle title: String) -> [SpecificCat] {
        switch title {
        case "HIKING":
            return getHikingGear()
        case "DIVING":
            return getDivingGear()
        case "FISHING":
            return getFishingGear()
        case "RV LIFE":
            return getRvLifeGear()
        case "BACKPACKS":
            return getBackpackingGear()
        default:
            return getCampingGear()
        }
    }

}
