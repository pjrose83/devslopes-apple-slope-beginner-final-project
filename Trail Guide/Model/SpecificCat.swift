//
//  SpecificCat.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import Foundation

struct SpecificCat {
    private(set) var title: String
    private(set) var imageName: String
    private(set) var description: String

    init(title: String, imageName: String, description: String) {
        self.title = title
        self.imageName = imageName
        self.description = description
    }
}
