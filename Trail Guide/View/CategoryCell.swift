//
//  CategoryCell.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryCellBackground: UIImageView!
    @IBOutlet weak var categoryCellLabel: UILabel!
    
    func updateViews(category: Category) {
        categoryCellBackground.image = UIImage(named: category.imageName)
        categoryCellLabel.text = category.title
    }

}
