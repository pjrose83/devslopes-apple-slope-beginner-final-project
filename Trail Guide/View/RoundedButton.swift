//
//  RoundedButton.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import UIKit

@IBDesignable

class RoundedButton: UIButton {
    @IBInspectable var cornerRadius:CGFloat = 10.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
