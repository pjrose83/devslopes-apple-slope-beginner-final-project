//
//  SecificCatCell.swift
//  Trail Guide
//
//  Created by Peter Rose on 17/04/2018.
//  Copyright © 2018 Peter Rose. All rights reserved.
//

import UIKit

class SpecificCatCell: UICollectionViewCell {
    
    @IBOutlet weak var specificCatImg: UIImageView!
    
    func updateViews(specificCat: SpecificCat) {
        specificCatImg.image = UIImage(named: specificCat.imageName)
    }
}
